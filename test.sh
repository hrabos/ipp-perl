#!/usr/bin/env bash

# pouziti:   is_it_ok.sh xlogin01-XYZ.zip testdir
#  
#   - POZOR: obsah adresare zadaneho druhym parametrem bude VYMAZAN!
#   - rozbali archiv studenta xlogin01-XYZ.zip do adresare testdir a overi formalni pozadavky pro odevzdani projektu IPP

# Autor: Michal Hruban

LOG=test.log

# extrakce archivu
function unpack_archive () {
	local ext=`echo $1 | cut -d . -f 2,3`
  echo -n "Archive extraction: "
  RETCODE=100  
	if [[ "$ext" = "zip" ]]; then
		unzip -o $1 >> $LOG 2>&1
    RETCODE=$?
	elif [[ "$ext" = "tgz" ]]; then
		tar xfz $1 >> $LOG 2>&1
    RETCODE=$? 
	fi
  if [[ $RETCODE -eq 0 ]]; then
    echo "OK"
  elif [[ $RETCODE -eq 100 ]]; then
    echo "ERROR (unsupported extension)"
    exit 1
  else
    echo "ERROR (code $RETCODE)"
    exit 1
  fi
} 
#   Priprava testdir
cp $1 $2 2>/dev/null

#   Overeni serveru (ala Eva neni Merlin)
echo -n "Testing on Merlin: "
HN=`hostname`
if [[ $HN = "merlin.fit.vutbr.cz" ]]; then
  echo "Yes"
else
  echo "No"
fi

#   Kontrola jmena archivu
cd $2
touch $LOG
ARCHIVE=`basename $1`
NAME=`echo $ARCHIVE | cut -d . -f 1 | egrep "^x[a-z]{5}[0-9][0-9a-z]-(CHA|CST|CSV|DKA|JSN|MKA|SYN|XTD|XQR)$"`
TASK=`echo $ARCHIVE | cut -d . -f 1 | sed 's/^x[a-z]\{5\}[0-9][0-9a-z]-\(CHA\|CST\|CSV\|DKA\|JSN\|MKA\|SYN\|XTD\|XQR\)$/\1/'`
echo -n "Archive name ($ARCHIVE): "
if [[ -n $NAME ]]; then
  echo "OK"
else
  echo "ERROR (the name $NAME does not correspond to a login + task identifier)"
fi

#   Extrakce ID ulohy ($TASK)
echo -n "Recognizing task ($TASK): "
TASKLEN=`echo -n $TASK | wc -m`
if [[ $TASKLEN = "3" ]]; then
  echo "OK"
else
  echo "ERROR ($TASK [$TASKLEN] not recognized)"
  exit 1
fi

#   Extrahovat do testdir
unpack_archive ${ARCHIVE}

echo "Project execution test: $SCRIPT"
#   Spusteni skriptu 
SCRIPT=`echo $TASK | tr [:upper:] [:lower:]`
TEST_SCRIPT=_stud_tests.sh
if [[ -f $SCRIPT.pl ]]; then
   chmod +x $TEST_SCRIPT
   ./$TEST_SCRIPT
   RETCODE=$?
   if [[ $RETCODE -eq 0 ]]; then
     echo "$SCRIPT RETURNED OK"
   else
     echo "$SCRIPT RETURNED ERROR (code $RETCODE)"
     exit 1
   fi
else
  echo "ERROR ($SCRIPT.pl|py not found)"
fi

#   Kontrola rozsireni
echo -n "Presence of file rozsireni (optional): "
if [[ -f rozsireni ]]; then
  echo "Yes"
  echo -n "Unix end of lines in rozsireni: "
  dos2unix -n rozsireni rozsireni.lf >> $LOG 2>&1
  diff rozsireni rozsireni.lf >> $LOG 2>&1
  RETCODE=$?
  if [[ $RETCODE = "0" ]]; then
    echo "OK"
  else
    echo "ERROR (CRLFs)"
  fi
else
  echo "No"
fi 
