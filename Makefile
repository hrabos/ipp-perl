# Project: SYN (Syntax highlighting)
# Author: Michal Hruban (xhruba00@stud.fit.vutbr.cz)
# Date of create: 19. 3. 2013 (little endian)
#
# Makefile

.PHONY:	all clean pack verify test

# Identification of the task.
LOGIN=	xhruba00
TASK=	SYN
EXT=	tgz
ARCHIVE=$(LOGIN)-$(TASK).$(EXT)	# the name of the archive

# SOURCE AND DOCUMENTATION SOURCE FILES.
SRC=	src/*.pm src/*.pl		# all python source files
DOC=	doc/$(TASK)-doc.pdf		# only final pdf file
EXF=	rozsireni			# extension file

# Auxiliary variables.
ARCHDIR=	pack.dir		# a directory for an archiving
VERIFYDIR=	verify.dir		# a directory for a verification
VERIFY=		./is_it_ok.sh	# a verification script
TESTDIR=	test.dir		# a directory for a testing
TEST_DATA=	test/_stud_tests.sh test/*.in test/*.out test/*.fmt test/empty test/*.c
TEST=		./test.sh		# a testing script (modified from IPP version)
MAKEDOC=	make --directory=doc --makefile=Makefile # command for documentation Makefile
WIS_DEBUG=	./wis.sh xhruba00 8675 41464 $(ARCHIVE) <<< "crat4rop"
WIS_RELEASE=	./wis.sh xhruba00 8675 41459 $(ARCHIVE) <<< "crat4rop"

# Default entrypoint do everything.
all:	doc-release pack verify

wis-debug:	all
	@echo :::
	@echo : WIS uploader
	@echo : debug version
	@echo :
	@$(WIS_DEBUG)

wis-release:	all
	@echo :::
	@echo : WIS uploader
	@echo : debug version
	@echo :
	@$(WIS_RELEASE)

# Create a TGZ archive.
pack:	clean
	@echo :::
	@echo : Create an archive.
	@echo :
	@mkdir $(ARCHDIR)
	@cp $(SRC) $(ARCHDIR)
	@cp $(DOC) $(ARCHDIR) || true
	@cp $(EXF) $(ARCHDIR) || true
	@tar -c --to-stdout -C ./$(ARCHDIR) `ls $(ARCHDIR)` |gzip --best -c > $(ARCHIVE)
	@rm -rf $(ARCHDIR)
	@echo The archive was created with name \'`echo $(ARCHIVE) |cut -f1`\' and size `du -h $(ARCHIVE) |cut -f1` contains: 
	@tar -tf $(ARCHIVE)
	@echo

# A verification of the archive
verify:
	@echo :::
	@echo : Verify an archive.
	@echo :
	@mkdir $(VERIFYDIR)
	@chmod +x $(VERIFY)
	@$(VERIFY) $(ARCHIVE) $(VERIFYDIR)
	@rm -rf $(VERIFYDIR)
	@echo

# Publish documentation
doc-release:
	@echo :::
	@echo : Publish documentation.
	@echo :
	$(MAKEDOC) release

# Publish documentation
doc:
	@echo :::
	@echo : draft documentation.
	@echo :
	$(MAKEDOC)

# A testing of python script
test:
	@echo :::
	@echo : Test a mka.py
	@echo :
	@rm -rf $(TESTDIR)
	@mkdir $(TESTDIR)
	@cp $(TEST_DATA) $(TESTDIR)
	@chmod +x $(TEST)
	@$(TEST) $(ARCHIVE) $(TESTDIR)
	@echo

# Clean routines.
clean:
	@rm -rf $(ARCHIVE) $(ARCHDIR) $(VERIFYDIR) $(TESTDIR)
