#!/usr/bin/env bash

# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# IPP - syn - veřejné testy - 2012/2013
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
# Činnost: 
# - vytvoří výstupy studentovy úlohy v daném interpretu na základě sady testů
# =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

TASK=syn
INTERPRETER=perl
EXTENSION=pl
#INTERPRETER=python3
#EXTENSION=py

RUN="$INTERPRETER $TASK.$EXTENSION"

# cesta pro ukládání chybového výstupu studentského skriptu
OUT="./out"
ERR="./err"
mkdir $OUT $ERR

function check_test () {
    local name=$1
    local exp_rv=$2
    local rv=$3
    if [[ $rv -ne $exp_rv ]]; then
        echo "  Returned value '$rv' is different from expected '$exp_rv'."
        return
    elif [[ $rv -eq 0 ]]; then
	    if [[ ! -e $OUT/$name.out ]]; then
	        echo "  Program output '$OUT/$name.out' does not exist."
	        return
	    else
	        diff_out=diff.out
	        diff $name.out $OUT/$name.out > $diff_out
	        diff_rv=$?
	        if [[ $diff_rv -ne 0 ]]; then
	            echo "  Program output is different from expected."
	            cat $diff_out
	            rm -f $diff_out
	            return
	    	fi
	    	rm -f $diff_out
	    fi
	fi
	echo "  OK"
    echo
}


echo "test01: Argument error"
$RUN --error 2> $ERR/test01.err
check_test test01 1 $?

echo "test02: Input error"
$RUN --input=nonexistent --output=$OUT/test02.out 2> $ERR/test02.err
check_test test02 2 $?

echo "test03: Output error"
$RUN --input=empty --output=nonexistent/$OUT/test03.out 2> $ERR/test03.err
check_test test03 3 $?

echo "test04: Format table error - nonexistent parameter"
$RUN --input=empty --output=$OUT/test04.out --format=error-parameter.fmt 2> $ERR/test04.err
check_test test04 4 $?

echo "test05: Format table error - size"
$RUN --input=empty --output=$OUT/test05.out --format=error-size.fmt 2> $ERR/test05.err
check_test test05 4 $?

echo "test06: Format table error - color"
$RUN --input=empty --output=$OUT/test06.out --format=error-color.fmt 2> $ERR/test06.err
check_test test06 4 $?

echo "test07: Format table error - RE syntax"
$RUN --input=empty --output=$OUT/test07.out --format=error-re.fmt 2> $ERR/test07.err
check_test test07 4 $?

echo "test08: Empty files"
$RUN --input=empty --output=$OUT/test08.out --format=empty 2> $ERR/test08.err
check_test test08 0 $?

echo "test09: Format parameters"
$RUN --input=basic-parameter.in --output=$OUT/test09.out --format=basic-parameter.fmt 2> $ERR/test09.err
check_test test09 0 $?

echo "test10: Argument swap"
$RUN --format=basic-parameter.fmt --output=$OUT/test10.out --input=basic-parameter.in 2> $ERR/test10.err
check_test test10 0 $?

echo "test11: Standard input/output"
$RUN --format=basic-parameter.fmt >$OUT/test11.out <basic-parameter.in 2> $ERR/test11.err
check_test test11 0 $?

echo "test12: Basic regular expressions"
$RUN --input=basic-re.in --output=$OUT/test12.out --format=basic-re.fmt 2> $ERR/test12.err
check_test test12 0 $?

echo "test13: Special regular expressions"
$RUN --input=special-re.in --output=$OUT/test13.out --format=special-re.fmt 2> $ERR/test13.err
check_test test13 0 $?

echo "test14: Special RE - symbols"
$RUN --input=special-symbols.in --output=$OUT/test14.out --format=special-symbols.fmt 2> $ERR/test14.err
check_test test14 0 $?

echo "test15: Negation"
$RUN --input=negation.in --output=$OUT/test15.out --format=negation.fmt 2> $ERR/test15.err
check_test test15 0 $?

echo "test16: Multiple format parameters"
$RUN --input=multiple.in --output=$OUT/test16.out --format=multiple.fmt 2> $ERR/test16.err
check_test test16 0 $?

echo "test17: Spaces/tabs in format parameters"
$RUN --input=multiple.in --output=$OUT/test17.out --format=spaces.fmt 2> $ERR/test17.err
check_test test17 0 $?

echo "test18: Line break tag"
$RUN --input=newlines.in --output=$OUT/test18.out --format=empty --br 2> $ERR/test18.err
check_test test18 0 $?

echo "test19: Overlap"
$RUN --input=overlap.in --output=$OUT/test19.out --format=overlap.fmt 2> $ERR/test19.err
check_test test19 0 $?

echo "test20: Perl RE"
$RUN --input=special-symbols.in --output=$OUT/test20.out --format=re.fmt 2> $ERR/test20.err
check_test test20 0 $?

echo "test21: Example"
$RUN --input=example.in --br --format=example.fmt > $OUT/test21.out 2> $ERR/test21.err
check_test test21 0 $?

echo "test22: Simple C program"
$RUN --input=cprog.c --br --format=c.fmt > $OUT/test22.out 2> $ERR/test22.err
check_test test22 0 $?

