#!perl

#SYN:xhruba00

package Exit;
$VERSION = v0.0.1;

# Package ExitCodes
# ===========================
# Responsible for a correct return value.
# Print a human readable return value.

use v5.8.8;
use strict;
use warnings;

# Exit function
# -print a reason
# -return a spcific value
sub Exit
{
    my($code, $message) = @_;
    print STDERR $message if defined $message;
    exit $code;
}

# General error codes
sub OK()
{
    Exit(0, "All seems to be OK.\n");
}

sub BAD_OPTION()
{
    Exit(1, "Wrong format of script's option.\nTry option --help\n");
}

sub INFILE()
{
    Exit(2, "Input file can't be used.\n");
}

sub OUTFILE()
{
    Exit(3, "Output file can't be used.\n");
}

sub BAD_FORMAT()
{
    Exit(4, "Wrong format of input file.\n");
}

1;

