#!perl

#SYN:xhruba00

package BaseInterface;
$VERSION = v0.0.1;

# Package BaseInterface
# ===========================
# All class are inherited from BaseInterface.
# It's used for debug.

use ExitCodes;
use vars qw(@ISA);
@ISA = qw(Exit);

use v5.8.8;
use strict;
use warnings;

sub new 
{
    my $class = shift;
    my $self = bless {}, $class;
    return $self;
}

# Logger print message into STDERR stream
# identify a caller
sub logger
{
    my $self = shift;
    my $class = ref $self;
    my $subrutine = (caller(1))[3];
    print STDERR ("# ", $subrutine, ": ", @_, "\n");
}

# Method dump() helps identify a class and object in debug mode.
sub dump
{
    my $self = shift;
    my $class = ref $self;
    $self->logger("%%% OBJECT DUMP %%%");
    $self->logger("class = '$class'");
    $self->logger("self = '$self'");
}

1;
