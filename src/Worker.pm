#!perl

#SYN:xhruba00

package Worker;
$VERSION = v0.0.1;

# Package Worker
# ===========================
# Worker is man who get it all together.
# For every recognized format rule replace text matched by regexp.

use BaseInterface;
use vars qw(@ISA);
@ISA = qw(BaseInterface);

use Formater;

use v5.8.8;
use strict;
use warnings;

########
# WORK #
########

sub new()
{
    my $class = shift;
    my $input = shift;
    my $format = shift;
    my $br = shift;
    my $self = $class->SUPER::new();
    bless $self, $class;
    
    $self->{"output"} = $self->parse($input, $format, $br); # autocall
    return $self;
}

sub parse()
{
    my($self, $input, $format, $br) = @_;
    my @formats = @{$format->{"formats"}};  # a list of format rules
    my %marks = ();                         # a hash of couples position marks

    # For every format rule replace a text by patterns.
    for(my $i=0; $i < scalar @formats; $i+=2)
    {
        $self->logger("Processing format rules:");
        my $exp = $formats[$i]->exp();
        my $r = $formats[$i+1];
        $self->logger("  expr = $exp");
        $self->logger("  rule = " . $r->left());

        # set a position on the start
        my $it_input = $input;
        while( $it_input =~ m/($exp)/sg ) # repeat for all matches
        {
            # mark only a nonzero's length strings
            if( length($1) > 0 )
            {
                $self->logger("    Find token: $1");
                my $l = length($1);
                my $p2 = pos($it_input); # a position of end
                my $p1 = $p2 - $l; # a position of start

                my $p1mark = "";
                my $p2mark = "";

                # remove and store existing value in a same position
                if( exists($marks{$p1}) ) {
                    $p1mark = delete($marks{$p1});
                }
                if( exists($marks{$p2}) ) {
                    $p2mark = delete($marks{$p2});
                }

                $marks{$p1} = $p1mark . $r->left(); # left part append on the last of string
                $marks{$p2} = $r->right() . $p2mark; # right part insert before a head of string
                $self->logger("      \$marks{$p1} = $marks{$p1}");
                $self->logger("      \$marks{$p2} = $marks{$p2}");
            }
        }

    }
    
    my $result = "";
    my $last_pos = 0;
    # Sort a marked position.
    foreach my $key (sort { $a <=> $b } keys(%marks))
    {
        # insert a origin text
        my $len = $key - $last_pos;
        if( $len ) {
            $result .= substr($input, $last_pos, $len);
        }
        # insert a tags
        $result .= $marks{$key};
        $last_pos = $key;
        $self->logger("input:\n", $input);
        $self->logger("STEP key = ", $key);
        $self->logger($result);
        $self->logger("");
    }
    # Insert a last part of origin text.
    $result .= substr($input, $last_pos);

    # Mode --br replace all '\n' on '<br />\n'
    if( $br )
    {
        $result =~ s/\n/<br \/>\n/g;
    }
    
    # Here is a completely filtered $input in $result.
    return $result;
}
