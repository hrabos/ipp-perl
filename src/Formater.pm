#!perl

#SYN:xhruba00

package Formater;
$VERSION = v0.0.1;

# Package Formater
# ===========================
# Read and parse a format rules.

use BaseInterface;
use vars qw(@ISA);
@ISA = qw(BaseInterface);

use Format;
use Rule;

use v5.8.8;
use strict;
use warnings;

sub new
{
    my $class = shift;
    my $filename = shift;
    my $self = $class->SUPER::new();
    bless $self, $class;
    $self->{"filename"} = ""; # a filename of format file
    my @array = ();
    $self->{"formats"} = \@array; # a list of formating rules
    $self->readFile($filename) if $filename; # autocall
    return $self;
}

# Reading of format file.
# Reads line and parse it.
sub readFile
{
    my($self, $filename) = @_;
    $self->{"filename"} = $filename;
    my @formats = ();
    open(FORMAT_FILE_HANDLE, $filename) or Exit->INFILE;
    while( <FORMAT_FILE_HANDLE> )
    {
        chomp($_);
        #### output ### regexp ######### format rule ############# mandatory syntax ##
        push(@formats, (Format->new($1), Rule->new($2))) if $_ =~ /^([^\t]+)\t+(.+)$/;
    }
    $self->{"formats"} = \@formats;
    close FORMAT_FILE_HANDLE;
}

sub dump
{
    my($self) = @_;    
    $self->SUPER::dump();
    $self->logger(".filename = $self->{'filename'}");
    my @formats = @{$self->{"formats"}};
    $self->logger(".formats[", (scalar @formats)/2, "] =");
    $self->logger("{");
    for(my $i=0; $i < scalar @formats; $i+=2)
    {
        my $exp = $formats[$i]->exp();
        my $ruleL = $formats[$i+1]->left();
        my $ruleR = $formats[$i+1]->right();
        $self->logger("\t$ruleL$exp$ruleR");
    }
    $self->logger("}");
}

1;
