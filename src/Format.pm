#!perl

#SYN:xhruba00

package Format;
$VERSION = v0.0.1;

# Package Format
# ===========================
# Encode a project regular exprression into a Perl RE.

use BaseInterface;
use vars qw(@ISA);
@ISA = qw(BaseInterface);

use v5.8.8;
use strict;
use warnings;

sub new()
{
    my $class = shift;
    my $input = shift;
    my $self = $class->SUPER::new();
    bless $self, $class;
    $self->{"exp"} = $self->parse($input); # autocall
    return $self;
}

# getter of 'exprression' property
sub exp()
{
    my($self) = @_;
    return $self->{"exp"};
}

sub parse()
{
    my($self, $input) = @_;
    $self->logger("I am replacing FORMAT '$input'");
    return $self->safe_input($input);
}

# Read input and escape all conflict characters.
sub safe_input()
{
    my($self, $input) = @_;
    
    # wrong combination of characters
    if( $input =~ /(\.\.|\.\+|\.\*|\.\||\|\.|!!|!\.|!\||!\*|!\+|!\(|\*\*|\*\+|\+\+|\+\*)/ ) {
        $self->logger("wrong combination: " . $1);
        Exit->BAD_FORMAT;
    }
    
    # encode \ ^ $ [ ] { } ? into safe form \x
    $input =~ s/(\\|\^|\$|\[|\]|\{|\}|\?)/\\$1/g;    
    
    # encode %% -> \x26 (SUB)
    $input =~ s/%%/\x26/g;
    
    my $output = $self->replaceDot($input); # autocall
    
    # decode \x26 (SUB) -> %
    $output =~ s/\x26/%/g;
    
    return $output;
}

sub replaceDot()
{
    my($self, $input) = @_;
    
    # A.B -> AB
    $input =~ s/([^%])\.(.)/$1$2/g;
    
    return $self->replaceNegation($input); # autocall
}

sub replaceNegation()
{
    my($self, $input) = @_;
    
    # !%a -> [^\w\W]
    $input =~ s/!%a/[^\\w\\W]/g;
    
    # !%l -> [^a-z]
    $input =~ s/!%l/[^a-z]/g;
    
    # !%L -> [^A-Z]
    $input =~ s/!%L/[^A-Z]/g;
    
    # !%W -> [^a-zA-Z0-9]
    $input =~ s/!%W/[^a-zA-Z0-9]/g;
    
    # !%w -> [^a-zA-Z]
    $input =~ s/!%w/[^a-zA-Z]/g;
    
    # !%d -> [^0-9]
    $input =~ s/!%d/[^0-9]/g;
    
    # !%(special_chars) -> to form [^%(special_chars)]
    $input =~ s/!(%[stn.|!*+()%])/\[^$1\]/g;
    
    # !. -> [^.]
    $input =~ s/!(.)/\[^$1\]/g;
    
    return $self->replaceSpecial($input); # autocall
}

# Replace a special characters.
sub replaceSpecial()
{
    my($self, $input) = @_;
    
    # %. -> \.
    $input =~ s/%([stn\.|*+()])/\\$1/g;
    
    # %a -> .
    $input =~ s/%a/\./g;

    # %l -> [a-z]
    $input =~ s/%l/[a-z]/g;
    
    # %L -> [A-Z]
    $input =~ s/%L/[A-Z]/g;
    
    # %W -> [a-zA-Z0-9]
    $input =~ s/%W/[a-zA-Z0-9]/g;
    
    # %w -> [a-zA-Z]
    $input =~ s/%w/[a-zA-Z]/g;
    
    # %d -> [0-9]
    $input =~ s/%d/([0-9]|0)/g;
    
    # %! -> !
    $input =~ s/%!/!/g;
    
    return $input;
}

sub dump
{
    my($self) = @_;    
    $self->SUPER::dump();
    $self->logger(".exp = $self->{'exp'}");
}

1;
