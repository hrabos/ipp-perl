#!perl

#SYN:xhruba00

###############
# ENTRY POINT #
###############

use v5.8.8;
use strict;
use warnings;

use Params; # get an option
use Worker; # main part of a filter

# Get a data from an user
my $opt = Params->new();
$opt->dump;

# Push an input data into the filter
my $worker = Worker->new($opt->{"input"}, $opt->{"format"}, $opt->{"br"});

# Write out a result.
$opt->writeOut($worker->{"output"});

Exit->OK;
