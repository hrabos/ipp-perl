#!perl

#SYN:xhruba00

package Params;
$VERSION = v0.0.1;

# Package Params
# ===========================
# Parse and store program option.
# Read input data and sotre them into self property.
# Control of output stream.

use BaseInterface;
use vars qw(@ISA);
@ISA = qw(BaseInterface);

use Formater;

use v5.8.8;
use strict;
use warnings;

# A package Params parses program's arguments and stores they in self.
# List of params:
# --help                Print a help message.
# --format=filename     The file contains a formating rules.
# --input=filename      The input file.
# --output=filename     The output file.
# --br                  Print extra <br />.
#

use Getopt::Long qw(Configure GetOptions);

# Constructor of Package
sub new {
    my $class = shift;
    my $self = $class->SUPER::new();
    bless $self, $class;
    $self->getOpt(); # autocall
    return $self;
}

# Parsing arguments.
sub getOpt {
    my($self) = @_; # no params
    
    # arrays for options detection
    my @help;
    my @format;
    my @input;
    my @output;
    my @br;
    
    my $rc = GetOptions(
        "help" => \@help,
        "format=s" => \@format,
        "input=s" => \@input,
        "output=s" => \@output,
        "br" => \@br
    );
    
    # Every option must be unique.
    if( not $rc or
        scalar @help > 1 or
        scalar @format > 1 or
        scalar @input > 1 or
        scalar @output > 1 or
        scalar @br > 1
    ){
        Exit->BAD_OPTION;
    }
    
    # Option --help cannot be combined with other options.
    if( scalar @help and (
            scalar @format or
            scalar @input or
            scalar @output or
            scalar @br
    )){
        Exit->BAD_OPTION;
    }
    
    # If Help is called, print it and quit.
    if( scalar @help )
    {
        printHelp();
        Exit->OK;
    }
    
    # Store information like scalars.
    $self->{"format"} = Formater->new(((scalar @format) ? $format[0] : 0));
    $self->{"input"} = $self->readIn(((scalar @input) ? $input[0] : 0));
    $self->{"output"} = (scalar @output) ? $output[0] : 0;
    $self->checkOut($self->{"output"});
    $self->{"br"} = (scalar @br) ? $br[0] : 0;
}

# Check qualities of output stream.
sub checkOut()
{
    my($self, $filename) = @_;
    if( $filename )
    {
        if( -e $filename and ! -w $filename ) {
            Exit->OUTFILE;
        }
    }
}

# Getting all input data in a huge block.
sub readIn()
{
    my($self, $filename) = @_;
    my $input = "";
    if( $filename )
    {
        if( ! -e $filename or ! -r $filename ) {
            Exit->INFILE;
        }
        open(INPUT, $filename) or Exit->INFILE;
        my $line = "";
        while( $line = <INPUT> )
        {
            $input .= $line;
        }
    }
    else
    {
        my $line = "";
        while( $line = <STDIN> )
        {
            $input .= $line;
        }
    }
    
    return $input;
}

# Controller of output stream.
sub writeOut()
{
    my $self = shift;
    my $filename = $self->{"output"};
    if( $filename ) {
        open(OUTPUT, ">$filename") or Exit->OUTFILE;
        print OUTPUT @_;
    }
    else {
        print STDOUT @_;
    }
}

sub dump {
    my($self) = @_; # no params
    $self->SUPER::dump();
    $self->logger(".input = $self->{'input'}") if $self->{"input"};
    $self->logger(".output = $self->{'output'}");
    $self->logger(".br = $self->{'br'}");
    $self->logger(".format =");
    $self->logger("{");
    $self->{'format'}->dump();
    $self->logger("}");

}

# Print the help message in czech language.
sub printHelp {
    print <<HELP;
SYN: Zvýraznění syntaxe
autor: Michal Hruban (xhruba00\@stud.fit.vutbr.cz)
datum: 14. 2. 2013

    • --help
        viz společné zadání všech úloh
 
    • --format=filename
        Určení formátovacího souboru. Soubor bude obsahovat libovolné množství
        formátovacích záznamů. Formátovací záznam se bude skládat z regulárního
        výrazu vymezujícího formátovaný text a příkazů pro formátování tohoto
        textu. Detailní popis viz níže.

    • --input=filename
        Určení vstupního souboru v kódování UTF-8.

    • --output=filename
        Určení výstupního souboru opět v kódování UTF-8 s naformátovaným
        vstupním textem. Formátování bude na výstupu realizováno některými HTML
        elementy na popis formátování. Regulární výrazy ve formátovacím souboru
        určí, který text se naformátuje, a formátovací informace přiřazené
        k danému výrazu určí, jakým způsobem se tento text naformátuje.

    • --br
        Přidá element <br /> na konec každého řádku původního vstupního textu
        (až po aplikaci formátovacích příkazů).
HELP
}

1;
