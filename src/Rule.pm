#!perl

#SYN:xhruba00

package Rule;
$VERSION = v0.0.1;

# Package Rule
# ===========================
# Read and parse a format style rule.
# Encode bold into <b> and </b> etc.

use BaseInterface;
use vars qw(@ISA);
@ISA = qw(BaseInterface);

use v5.8.8;
use strict;
use warnings;

sub new()
{
    my $class = shift;
    my $input = shift;
    my $self = $class->SUPER::new();
    bless $self, $class;
    $self->{"left"} = "";
    $self->{"right"} = "";
    $self->parse($input); # autocall
    return $self;
}

# Getter of left part <TAG>
sub left()
{
    my($self) = @_;
    return $self->{"left"};
}

# Getter of right part </TAG>
sub right()
{
    my($self) = @_;
    return $self->{"right"};
}

sub parse()
{
    my($self, $input) = @_;
 
    my @properties = split(',', $input);
    my @left_resulties = ();    # a list of left parts
    my @right_resulties = ();   # a list of right parts

    foreach my $p (@properties)
    {
        # check a syntax
        if( $p =~ /\s*(bold|italic|underline|teletype|size:[1-7]|color:[0-9A-F]{6})\s*/ ) {
            my $r = $1;
            # BOLD
            if( $r =~ /bold/ ) {
                push(@left_resulties, "<b>");
                unshift(@right_resulties, "</b>");
            }
            # ITALIC
            elsif( $r =~ /italic/ ) {
                push(@left_resulties, "<i>");
                unshift(@right_resulties, "</i>");
            }
            # UNDERLINE
            elsif( $r =~ /underline/ ) {
                push(@left_resulties, "<u>");
                unshift(@right_resulties, "</u>");
            }
            # TELETYPE
            elsif( $r =~ /teletype/ ) {
                push(@left_resulties, "<tt>");
                unshift(@right_resulties, "</tt>");
            }
            # SIZE:x
            elsif( $r =~ /size:([1-7])/ ) {
                push(@left_resulties, "<font size=$1>");
                unshift(@right_resulties, "</font>");
            }
            # COLOR:xxyyzz
            elsif( $r =~ /color:([0-9A-F]{6})/ ) {
                push(@left_resulties, "<font color=#$1>");
                unshift(@right_resulties, "</font>");
            }
        }
        # syntax error
        else
        {
            Exit->BAD_FORMAT;
        }
    }
    
    # Store rules.
    $self->{"left"} = join("", @left_resulties);
    $self->{"right"} = join("", @right_resulties);
}
